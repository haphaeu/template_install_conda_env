# Intro

Template for a python application installer and launcher using conda environments in windows.

# How to use

## Development

 1. Develop the application in a bare conda environment, installing only the required packages

    `conda create --name myenv`
    `conda install -n myenv list_of_packages_required`

 2. Update the launcher and installer batch files `myapp.bat` and `install.bat` with the name of your application and environment.

 3. When ready for deployment, create a file with the specs of the environment

    `conda list --explicit > spec-file.txt`


## Installation

In the client machine, install the environment by running the installer batch file `install.bat`


## Launching the application

After successful installation of the environment, the application can be launched using the launcher batch file.

## Uninstalling

To uninstall the application, just delete the filed.

To remove the conda environment created by the installer, just run:

    `conda env remove --name myapp`

In the command above, replace `myapp` by the name of the application.
